/*Observa��es:
- Utilizar arrays.
- Criar um menu de op��es para que as funcionalidades acima, incluindo a op��o de sa�da pelo usu�rio.*/

package Exercicio_02;

import java.util.Scanner;
import java.util.*;

public class Conta{
    
    //Constante
    //static final int MAX = 20;
   
    //vari�vel comum
    static int index = 0;
    
    //Lista de contas
    //static Conta[] lista = new Conta[MAX];
    
  //Estrutura de dados ArrayList Gen�rica
    static List<Operacoes> lista = new ArrayList<Operacoes>();
    
    static Scanner tecla = new Scanner(System.in);

    public static void main(String[] args) {
        int op;
        do {                
            System.out.println("*** MENU PRINCIPAL ***");
            System.out.println("1-Incluir Conta");
            System.out.println("2-Excluir Conta");
            System.out.println("3-Realizar Dep�sito");
            System.out.println("4-Sacar");
            System.out.println("5-Listar saldo das Contas");
            System.out.println("6-Transfer�ncia");
            System.out.println("7-Sair");
            System.out.println("Digite sua op��o: ");
            op = tecla.nextInt(); 
            switch(op){
                case 1: incluirConta(); break;
                case 2: excluirConta(); break;
                case 3: depositarValor(); break;    
                case 4: sacarValor(); break;
                case 5: listarContas(); break;
                case 6: TransferirValor(); break;
                case 7: break;
            }
        } while (op!=7);       
    }
    
	public static void incluirConta(){
        //Entrada
        System.out.println("Digite o n�mero da conta:");
        int num = tecla.nextInt();
        System.out.println("Digite o saldo da conta:");
        double saldo = tecla.nextDouble();
        
        //Criar o objeto e inserir na lista
        //lista[index++] = new Operacoes(num, saldo);
        lista.add(new Operacoes(num, saldo));
        System.out.println("Conta cadastrada com sucesso!");
    }
    
	 private static void excluirConta() {
		//Entrada
		 	System.out.println("Qual conta deseja excuir? ");   
		 	System.out.println("Digite o n�mero da conta: ");
	        int num = tecla.nextInt();
	        
	        for (Operacoes item : lista) {
	            if (item.getNumero() == num){
	               lista.remove(item);
	            }
	        }
		}
	
	 public static void depositarValor(){
	        //Entrada
	        System.out.println("Digite o n�mero da conta:");
	        int num = tecla.nextInt();
	        System.out.println("Digite o valor do dep�sito:");
	        double valor = tecla.nextDouble();
	        
	        //Procurar a conta na lista
	        for (Operacoes item : lista) {
	            if (item.getNumero() == num){
	                item.depositar(valor);
	                break;
	            }
	        }
	    }
	 
	public static void sacarValor(){
        //Entrada
        System.out.println("Digite o n�mero da conta:");
        int num = tecla.nextInt();
        System.out.println("Digite o valor do saque:");
        double valor = tecla.nextDouble();
        
        //Procurar a conta na lista
        for (Operacoes item : lista) {
            if (item.getNumero() == num){
                item.sacar(valor);
                break;
            }
        }
    }
    
    public static void listarContas(){
        double total = 0;
        System.out.println("N� Conta:........ SALDO:");
        for (Operacoes item : lista) {
            System.out.println("|" + item.getNumero()
                               + "........" +
                               item.getSaldo() + "|");
            total = total + item.getSaldo();
        }
        System.out.println("----------------------------");
        System.out.println("| Total:........" + total + "|");
        System.out.println("----------------------------");
        System.out.println("Total:........" + total);
    }
    
    private static void TransferirValor() {
    	//Entrada
        System.out.println("Para qual conta deseja realizar a tranfer�ncia?");
        System.out.println("Por favor, forne�a o n�mero: ");
        int num = tecla.nextInt();
        System.out.println("Qual o valor que deseja transferir?:");
        double valor = tecla.nextDouble();
        
        //Procurar a conta na lista
        for (Operacoes item : lista) {
            if (item.getNumero() == num){
                item.transferir(valor);
                break;
            }
        }
	}
    
}
