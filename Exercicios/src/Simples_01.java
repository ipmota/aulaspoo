import java.util.*;

public class Simples_01 {
	//declaracao de variaveis globais estaticas
	static double raio, area;
	static Scanner entrada = new Scanner(System.in);
	
	public static void main(String[] args) {
		//atribui��o de valores
		raio = 0;
		area = 0;
		
		//entrada de dados
		System.out.println("Informe o raio do c�rculo: ");
		raio = entrada.nextFloat();
		
		area = area();
		
		//saida de informa��es
		System.out.println("A �rea do c�rculo �: " +area);
	}
	
	public static double area() {
		//processamento
			return 3.14 * Math.pow(raio, 2); //Pi=3,14
	}
	
}