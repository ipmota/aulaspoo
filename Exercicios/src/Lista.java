/*Fa�a um programa, utilizando a classe Pessoa, que permita:
1. Inserir pessoas em uma lista, com os dados lidos via teclado.
2. Listar todas as pessoas armazenadas.

Observa��es:
- Apresente um menu para o usu�rio de forma que ele possa realizar estas opera��es (Inserir ou Listar) repetidas vezes, at� que deseje sair.
*/

public class Lista extends Pessoa{

	//Atributos de inst�ncia
    private String nome;
  
    //M�todos de acesso
    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
    
    //construtor
    public Lista(String nome) {
    	this.nome = nome;
    }
}