import java.util.*;
public class Simples_04 {

	//declara��o de variaveis estaticas
	static double pot_lamp, larg_com, comp_com, area_com, pot_total, num;
	static int num_lamp;
	static Scanner a = new Scanner(System.in);
	
	public static void main(String[] args) {
		//entrada de dados
		System.out.println("Qual a pot�ncia da l�mpada, em watts?");
		pot_lamp = a.nextDouble();
		System.out.println("Qual a largura do c�modo, em metros?");
		larg_com = a.nextDouble();
		System.out.println("Qual o comprimento do c�modo, em metros?");
		comp_com = a.nextDouble();
	
		num_lamp = lampada();

		//sa�da de dados
		System.out.println("N�mero de l�mpadas necess�rias para iluminar esse c�modo: " +num_lamp);
	}
	public static int lampada() {
		area_com = larg_com * comp_com;
		pot_total = area_com * 18;
		num = (pot_total/pot_lamp);
		num_lamp = (int)num;
		return num_lamp;
	}

}
