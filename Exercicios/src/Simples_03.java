import java.util.Scanner;

public class Simples_03 {
	//declara��o de vari�veis globais est�ticas
	static double temp_f, temp_c;
	static Scanner entrada = new Scanner(System.in);
	
	public static void main(String[] args) {
		//atribui��o de valores	
		temp_f = 0;
		temp_c = 0;

		//entrada de dados
		System.out.println("Informe a temperatura em graus Celcius: ");
		temp_c = entrada.nextDouble();
	
		temp_f = temperatura();
	
		//sa�da de dados
		System.out.println("A temperatura em Fahrenheit �:" +temp_f);

	}
	public static double temperatura() {
		temp_f = ((temp_c/5)* 9)+32;
		return temp_f;
	}
	
}