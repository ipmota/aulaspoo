import java.util.Scanner;

public class Simples_02 {
	//declara��o de variaveis globais estaticas
	/*temp_f: temperatura Fahrenheit
	 *temp_c: temperatura Celcius*/
	static double temp_f, temp_c;
	static Scanner entrada = new Scanner(System.in);
	
		public static void main(String[] args) {
			//atribui��o de valores	
			temp_f = 0;
			temp_c = 0;
	
			//entrada de dados
			System.out.println("Informe a temperatura em graus Fahrenheit: ");
			temp_f = entrada.nextDouble();
	
			temp_c = temperatura();
	
			//sa�da de dados
			System.out.println("A temperatura em Celsius �:" +temp_c);
		}
	
		public static double temperatura() {
			temp_c = ((temp_f - 32) * 5) / 9;
			return temp_c;
		}
}